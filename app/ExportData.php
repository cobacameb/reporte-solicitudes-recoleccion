<?php 
$usuario = $_COOKIE["usuario_plight"];
$oficina = $_COOKIE["oficina_plight"];
$fecha = date('d-m-Y  h:i A');
if($usuario!=""){
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Exportar informacion </title>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/plain; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"></link>
		<link rel="stylesheet" href="./assets/styles/style.css">
		<script type="text/javascript">
			function Print(argument) {
				window.print();
			}
		</script>
	</head>
	<body>
		<div class="btn-group">
			<button type="button" class="btn btn-info btn-lg dropdown-toggle" data-toggle="dropdown">Guardar/Imprimir<span class="caret"></span></button>
			<ul class="dropdown-menu" role="menu">
				<li><a onclick="exportAll('csv');" href="javascript://"><img src="./assets/imgs/csv_text_opt.png">    CSV</a></li>          
				<li><a onclick="exportAll('xls');" href="javascript://"><img src="./assets/imgs/xls_opt.png">  XLS </a></li>   
				<li><a onclick="Print();" href="javascript://"><img src="./assets/imgs/printer_opt.png">   Imprimir  </a></li>
			</ul>
		</div>
		<br>
		<div class="table">
			<table id="firm_table" class="table table-striped table-bordered table-hover">
				<thead >
					<tr>
						<td colspan="3" style="font-size:12px;font-weight:bold;">
							<span >Usuario :&nbsp;</span><span ><?echo"$usuario";?></span>
						</td>
						<td colspan="3" style="font-size:12px;font-weight:bold;">
							<span >Fecha/hora :&nbsp;</span><span ><?echo"$fecha";?></span>
						</td>
						<td colspan="3" align="right"  style="font-size:12px;font-weight:bold;">
							<span >Oficina :&nbsp;</span><span ><?echo"$oficina";?></span>
						</td>
					</tr>
					<tr>
						<th>Cliente Premium</th>
						<th>Solicitud</th>
						<th><center> Fecha Captura</center></th>
						<th><center> Oficina </center> </th>
						<th><center> Ciudad </center> </th>
						<th><center> Nombre <br> Cliente</center></th>
						<th><center> Domicilio </center></th>
						<th><center> Telefono</center> </th>
						<th><center> Fec Recol</center></th>
						<th><center> Capturo</center></th>
						<th><center> Datos <br> del <br> destino </center></th>
						<th><center> Recibida </center></th>
						<th><center> Revisada</center></th>
						<th><center>Asignada</center></th>
						<th><center> Orden <br> Recolección </center></th>
						<th><center>Chofer <BR> Recol </center></th>
						<th><center> Rec Digital <br> Efectuada</center></th>
						<th><center>Excepcion <br> Digital</center></th>
						<th><center> Cerrada <br> Con <br> O/E </center></th>
						<th><center>Cancelada</center></th>	
						<th><center> ORM</center></th>
						<th><center> Guía</center></th>
					</tr>
				</thead>
				<tbody>
					<?
					$cad=json_decode($griddatos);
					foreach ($cad as $key => $value) {
						$numeorden= $value->NUMEORDEN;
						$premium= $value->PREMIUM;
						$numero= $value->NUMERO;
						$chofasig= $value->CHOFASIG;
						$cve_choferllega= $value->CVE_CHOFERLLEGA;
						$fechallega= $value->FECHALLEGA;
						$horallega= $value->HORALLEGA;
						$geopos= $value->GEOPOS;
						$geopos_excep= $value->NUMERO;
						$hora_excep= $value->NUMEORDEN;
						$ormllega = $value->ORMLLEGA;
						$ofillega= $value->NUMERO;
						$foliollega= $value->NUMEORDEN;
						$oficina_recolec= $value->PREMIUM;
						$numguia= $value->NUMGUIA;
						$fecha= $value->FECHA;
						$ciudad= $value->CIUDAD;
						$nomcliente= $value->NOMCLIENTE;
						$domicilioynum= $value->DOMICILIOYNUM;
						$fecha_recolec= $value->FECHA_RECOLEC;
						$hora_recolec= $value->HORA_RECOLEC;
						$horainiciofin= $value->HORAINICIOFIN;
						$hora_recolecini= $value->HORA_RECOLECINI;
						$hora_recolecfin= $value->HORA_RECOLECFIN;
						$cancelada= $value->CANCELADA;
						$revisada= $value->REVISADA;
						$asignada= $value->ASIGNADA;
						$destinatario= $value->DESTINATARIO;
						$capturo= $value->CAPTURO;
					?>
					<tr>
						<td><?echo "$premium"; ?></td>
						<td><?echo "$numero"; ?> </td> 
						<td><?echo "$fecha"; ?> </td>     
						<td><?echo "$oficina_recolec"; ?></td>
						<td><?echo "$ciudad"; ?> </td> 
						<td><?echo "$nomcliente"; ?> </td>   
						<td><?echo "$domicilioynum"; ?></td>
						<td><?echo "$tel_recolec"; ?> </td> 
						<td><?echo "$fecha_recolec"; ?> </td>   
						<td><?echo "$capturo"; ?></td>
						<td><?echo "$destinatario"; ?> </td> 
						<td><?echo "$recibida"; ?> </td>   
						<td><?echo "$revisada"; ?></td>
						<td><?echo "$asignada"; ?> </td> 
						<td><?echo "$numeorden"; ?> </td>   
						<td><?echo "$chofasig"; ?></td>
						<td><?echo "$geopos"; ?> </td> 
						<td><?echo "$geopos_excep"; ?> </td>   
						<td><?echo "$folio"; ?></td>
						<td><?echo "$cancelada"; ?> </td> 
						<td><?echo "$ormllega"; ?> </td>   
						<td><?echo "$numguia"; ?> </td>
					</tr>
					<?}?>
				</tbody>
			</table>
		</div>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
		<script src="Js/dist/tableExport.js"></script>
		<script src="./js/main.js"></script>
		<?}else{?>
		<script type="text/javascript">
			alert("Debes Iniciar Session el Portal Light");
		</script>
		<?}?>
	</body>
</html>