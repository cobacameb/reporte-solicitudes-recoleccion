<?php
// Conexion a bases de Oracle
class oracle
{

	var $nombre;
	var $database;
	var $columna;
	var $usuario;
	var $rows;
	var $sql;
	var $password;
	var $int;
	var $host;
	var $array;
	var $db;
	var $pointer;
	var $query;
	var $dir;

	function oracle()
	{
		$this->host = "192.168.120.132";
		$this->port = "1521";
		$this->database = "tpitic";
		$this->usuario = "bcobacamepiticsis";
		$this->password = "b0c0p1t1csis1234";
		/*echo "<script language='javascript'>alert('Oracle DB');</script>"; */
	}

	function setNombre($nom)
	{
		$this->nombre = $nom;
	}

	function setPassword($pass)
	{
		$this->password = $pass;
	}

	function setUsuario($usu)
	{
		$this->usuario = $usu;
	}

	function setHost($dirhost)
	{
		$this->host = $dirhost;
	}

	function getNombre()
	{
		return $this->nombre;
	}

	function getUsuario()
	{
		return $this->usuario;
	}

	function getPassword()
	{
		return $this->password;
	}

	function direccion()
	{
		$this->dir = $this->host . ":" . $this->port . "/" . $this->database;
		return $this->dir;
	}

	function conectar()
	{
		$dir = $this->direccion();
		$this->db = OCILogon($this->usuario, $this->password, $dir);
		if (!$this->db) {
			$this->db = var_dump(OCIError());
			die();
		}
		return $this->db;
	}


	function close($dbs)
	{
		OCILogoff($this->db);
	}

	function setDatabase($ndb)
	{
		$this->database = $ndb;
		if ($this->db > 0) {
			$this->close($this->db);
			$this->conectar();
		}
		return $this->database;
	}

	function setQuery($sqlstr)
	{
		$this->sql = $sqlstr;
		$this->query = OCIparse($this->db, $sqlstr);
		$error = OCIExecute($this->query);
		if (!$error) {
			$error = OCIError($this->query);
			$error = "Error: ${error['code']} ${error['message']}";
			return $error;
		} else {
			return $this->query;
		}
	}

	function getNumrows($res)
	{
		$this->rows = OCIFetchStatement($res, $this->array);
		OCIExecute($this->query, OCI_DEFAULT);
		return $this->rows;
	}

	function getArray($qu)
	{
		$this->int = OcifetchInto($this->query, $this->array, OCI_ASSOC);
		if ($this->int >= 1) {
			return $this->array;
		}
	}

	function movePointer($result, $row)
	{
		OCIExecute($result, OCI_DEFAULT);
		for ($i = 0; $i < $row; $i++) {
			OCIFetch($result);
		}
		return $this->pointer;
	}
	function setQueryAux($sqlstr)
	{
		$sql = $sqlstr;
		$this->query2 = OCIparse($this->db, $sqlstr);
		$error = OCIExecute($this->query2);
		if (!$error) {
			$error = OCIError($this->query2);
			$error = "Error: ${error['code']} ${error['message']}";
			return $error;
		} else {
			return $this->query2;
		}
	}

	function getNumrowsAux($res)
	{
		$rows = OCIFetchStatement($res, $array2);
		OCIExecute($this->query2);
		return $rows;
	}
	function getArrayAux($qu)
	{
		$int2 = OcifetchInto($qu, $otro, OCI_ASSOC);
		if ($int2 >= 1) {
			return $otro;
		}
	}
}
// Conexion a bases de MySQL
class mysql
{

	var $nombremy;
	var $databasemy;
	var $usuariomy;
	var $rowsmy;
	var $passwordmy;
	var $arraymy;
	var $hostmy;
	var $objetomy;
	var $dbmy;
	var $pointermy;
	var $querymy;

	function mysql()
	{
		$this->hostmy = "192.168.100.4";
		//echo "<script language='javascript'>alert('MySQL dbmy');<//script>";
	}

	function setNombre($nom)
	{
		$this->nombremy = $nom;
	}

	function setPassword($pass)
	{
		$this->passwordmy = $pass;
	}

	function setUsuario($usu)
	{
		$this->usuariomy = $usu;
	}

	function setHost($dirhostmy)
	{
		$this->hostmy = $dirhostmy;
	}

	function getNombre()
	{
		return $this->nombremy;
	}

	function getUsuario()
	{
		return $this->usuariomy;
	}

	function getPassword()
	{
		return $this->passwordmy;
	}


	function conectar()
	{
		if ($this->dbmy > 0) {
		} else {
			$this->dbmy = mysql_connect("$this->hostmy", "$this->usuariomy", "$this->passwordmy");
		}
		return $this->dbmy;
	}


	function close($dbmys)
	{
		mysql_close($dbmys);
	}

	function setDatabase($ndbmy)
	{
		if ($this->dbmy > 0) {
		} else {
			$this->conectar();
		}
		$this->databasemy = mysql_select_db($ndbmy);
		return $this->databasemy;
	}

	function setQuery($sqlstr)
	{
		$this->querymy = mysql_query("$sqlstr");
		return $this->querymy;
	}

	function getNumrows($res)
	{
		$this->rowsmy = mysql_num_rows($res);
		return $this->rowsmy;
	}

	function getArray($arre)
	{
		$this->arraymy = mysql_fetch_array($arre);
		return $this->arraymy;
	}

	function getObjeto($obj)
	{
		$this->objetomy = mysql_fetch_object($obj);
		return $this->objetomy;
	}

	function movePointer($result, $num)
	{
		$this->pointermy = mysql_data_seek($result, $num);
		return $this->pointermy;
	}
}

// Conexion a bases de Postgree
class PG
{

	var $nombrepg;
	var $databasepg;
	var $usuariopg;
	var $rowspg;
	var $passwordpg;
	var $arraypg;
	var $hostpg;
	var $sqlpg;
	var $dbpg;
	var $pointerpg;
	var $querypg;

	function PG()
	{
		$this->hostpg = "192.168.100.4";
		//echo "<script language='javascript'>alert('Postgree dbpg');</script>";
	}

	function setNombre($nom)
	{
		$this->nombrepg = $nom;
	}

	function setPassword($pass)
	{
		$this->passwordpg = $pass;
	}

	function setUsuario($usu)
	{
		$this->usuariopg = $usu;
	}

	function setHost($dirhostpg)
	{
		$this->hostpg = $dirhostpg;
	}

	function getNombre()
	{
		return $this->nombrepg;
	}

	function getUsuario()
	{
		return $this->usuariopg;
	}

	function getPassword()
	{
		return $this->passwordpg;
	}


	function conectar()
	{
		$this->dbpg = pg_connect("host=$this->hostpg dbname=$this->databasepg user=$this->usuariopg password=$this->passwordpg");
		return $this->dbpg;
	}

	function close($dbpgs)
	{
		pg_close($dbpgs);
	}

	function setDatabase($ndbpg)
	{
		$this->databasepg = $ndbpg;
		if ($this->dbpg > 0) {
			$this->close($this->dbpg);
			$this->conectar();
		}
		return $this->databasepg;
	}

	function getDatabase()
	{
		return $this->databasepg;
	}

	function setQuery($sqlpgstr)
	{
		$this->sqlpg = $sqlpgstr;
		$this->querypg = pg_exec($this->sqlpg);
		return $this->querypg;
	}

	function getNumrows($res)
	{
		$this->rowspg = pg_numrows($res);
		return $this->rowspg;
	}

	function getArray($arre)
	{
		$this->arraypg = pg_fetch_array($arre);
		return $this->arraypg;
	}

	function getObjeto($obj)
	{
		$this->objeto = pg_fetch_object($obj);
		return $this->objeto;
	}

	function movePointer($result, $row)
	{
		$this->querypg = pg_exec($this->sqlpg);
		for ($i = 0; $i < $row; $i++) {
			$this->getArray($result);
		}
		return $this->pointerpg;
	}
}
