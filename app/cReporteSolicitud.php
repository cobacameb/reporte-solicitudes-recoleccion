<?php
require_once('./clasesql.php');
class cReporteSolicitud
{

    private $id;
    private $descripcion;
    private $oficina;
    private $usuario;
    private $objSQL;
    private $db;
    public function __construct()
    {

        $this->objSQL = new oracle();
        $this->db =$this->objSQL->conectar();
    }
    public function getRegiones($user){
         
        $sql.= " SELECT distinct B.REGION_VTAS FROM CATALOGOS.GER_REGIONAL A 
        LEFT JOIN GUIAS.OFICINA B ON A.REGION = B.REGION_VTAS 
        WHERE A.USUARIOWEB = '$user' and b.ACTIVADA=1 and  b.OFIVENTAS IS NOT NULL ";
        $sql = strtoupper($sql);
     
        $result = $this->objSQL->setQuery($sql);
        $recno =$this->objSQL->getNumrows($result);
        $cur = 0;
        $data=array();
    while ($cur<$recno) {
        $row = $this->objSQL->getArray($result);
        $data[$cur] = $row;
        $cur++;
    }
 
        return $data;
    }
    public function consultar($region, $fechacap, $fechai, $fechaf, $estatussolicitud, $oficina, $incluircanceladas, $pendienteguia)
    {

        $sql = "Select DISTINCT (a.numero),b.NUMEORDEN, a.capturo,b.usuario as chofasig, x.cve_chofer as cve_choferllega, x.fecha as fechallega, x.hora as horallega,";
        $sql .="x.geopos , x.referencia as ormllega, x.OFICINA as ofillega,x.FOLIO as foliollega, a.oficina_recolec, a.GUIA,to_char( a.fecha,'yyyy-mm-dd') as fecha,";
        $sql .="a.ciudad_recolec as ciudad,a.nomcliente, a.domicilioynum, a.tel_recolec,to_char( a.hora_recolec,'hh24:mi:ss') as hora_recolec,";
        $sql .="to_char( a.hora_recolecini,'hh24:mi:ss')  ||'-' || to_char( a.hora_recolecfin,'hh24:mi:ss') horainiciofin,";
        $sql .="to_char( a.hora_recolecini,'hh24:mi:ss') as hora_recolecini,";
        $sql .="to_char( a.hora_recolecfin,'hh24:mi:ss') as hora_recolecfin,to_char( a.fecha_recolec,'yyyy-mm-dd') as fecha_recolec, a.CERRADA, a.CANCELADA,";
        $sql .="a.REVISADA, a.ASIGNADA, a.DESTINATARIO, p.PREMIUM,excep.geopos as geopos_excep,excep.hora as hora_excep";
        if ($region!='' and $region !='tod') {
            $sql.=" from PITIC.SOLIC_RECOL a left join Guias.oficina e on (a.OFICINA_RECOLEC = e.oficina )";
        } else {
            $sql .=" from PITIC.SOLIC_RECOL a";
        }
      
            $sql .=" left join PITIC.SOLIC_ENVIO b on a.numero=b.num_solicitud ";   
            $sql .=" LEFT JOIN USUARIO_PLIGHT.MOVIL_RECOLOK X on b.numeorden=x.folio";
            $sql .=" LEFT JOIN USUARIO_PLIGHT.MOVIL_RECOLEXCEP excep on b.numeorden=excep.folio";
            $sql .=" LEFT JOIN CATALOGOS.CLI_CORRECTO p ON (a.NOMCLIENTE = p.NOMBRE)";
            $sql .=" where a.numero>0 and ";
        if ($fechacap=="true") {
            $sql.= "  a.fecha between to_date('$fechai','yyyy-mm-dd') and to_date('$fechaf','yyyy-mm-dd') ";
        } else {
            $sql.= "  a.fecha_recolec between to_date('$fechai','yyyy-mm-dd') and to_date('$fechaf','yyyy-mm-dd') ";
        }
        if($pendienteguia =="true"){
            $sql.= " AND a.ASIGNADA = 1 AND a.CANCELADA <> 1 AND A.GUIA IS NULL AND x.geopos IS NOT NULL AND excep.geopos IS NULL ";
        }
        if ($incluircanceladas == "true") {
               $canceladas = " ";
        } else { 
            $canceladas = " AND  a.CANCELADA <> 1 ";
        }

        switch ($estatussolicitud) {
            case 'PEN':
                // $sql.=" AND (A.CERRADA <> 1 OR X.FOLIO is  null OR EXCEP.FOLIO is  null)  ".$canceladas;
                // 18-03-2022 Izo: SI LA SOLICITUD TIENE GUIA ENTONCES YA SE CONSIDERA COMO REALIZADA
                $sql.=" AND (X.FOLIO is  null) AND (A.GUIA IS NULL)  ".$canceladas;
                break;
            case '0':
                $sql.=" AND  a.CERRADA <> 1 AND  a.REVISADA <> 1 AND  a.ASIGNADA <> 1 ".$canceladas;
                break;
            case '1':
                $sql.=" AND  a.REVISADA = 1".$canceladas;
                break;
            case '2':
                $sql.=" AND  a.ASIGNADA = 1".$canceladas;
                break;
            case '3':
               // $sql.=" AND  a.CERRADA = 1".$canceladas;
         //  $sql.=" AND (X.FOLIO is not null OR EXCEP.FOLIO is  null)  ".$canceladas;
            $sql.=" AND (X.FOLIO is not null)  ".$canceladas;
                break;
            case '4':
                $sql.=" AND  a.CANCELADA = 1";
                break;
        }
        if ($region!='' and $region !='tod') {
            $sql.= " and e.REGION_ESTADIST = '$region' ";
        } else {
            if ($oficina !="OPE") {
                if ($oficina != "TOD" && $oficina!='MT1' && $oficina!='MTY') {
                    $sql.= "and oficina_recolec='$oficina' ";
                }
                if (($oficina=='MTY' && $oficina != "TOD") || ($oficina=='MT1' && $oficina != "TOD")) {
                    $sql.= "and oficina_recolec IN ('MTY','MT1') ";
                }
            }
        }
        
            $sql.= "order by  a.numero";
            $sql = strtoupper($sql);
	if($_COOKIE["usuario_plight"]=='iperez'){
		} 
            $result = $this->objSQL->setQuery($sql);
            $recno =$this->objSQL->getNumrows($result);
            $cur = 0;
            $data=array();
        while ($cur<$recno) {
            $row = $this->objSQL->getArray($result);
            $data[$cur] = $row;
            $cur++;
        }
     
            return $data;
    }
    public function consultaParseada($data, $oficina,$estatus)
    {
        $tot = count($data);
        for ($i=0; $i< $tot; $i++) {
            $llega_orm    = $data[$i]['ORMLLEGA'];
            $llega_folio  = $data[$i]['FOLIOLLEGA'];
            $llega_ofi    = $data[$i]['OFILLEGA'];
            $no = $data[$i]["NUMERO"];
            $llega_embarque="";
            $string_orm = "ORM";
            $llega_orden = ($llega_orm>' ') ? $llega_orm : $llega_folio ;
            if($llega_orden < ' '){
                $llega_orden = $no;
            }
            if ($llega_orden>' ') {
                    //sql para consultar datos extras de guia recorriendo el primer arreglo de datos $data, al mismo tiempo se hace un update para cerrar,
                    // en la funcion cerrar_recoleccion
                $sqlBuscaEMB ="Select FOLIO,RECOLECCION,NUMGUIA,FECHA as FECHAOEMB  from GUIAS.ORDENEMBARQUE where fecha > (sysdate- 500) ";
                $sqlBuscaEMB.= "and  RECOLECCION IN  ('$llega_orden','".$llega_ofi.$llega_orden."','".$string_orm.$llega_orden."')";
           // echo $sqlBuscaEMB;
                if ($oficina != "OPE") {
                    if ($oficina != "TOD" && $oficina!='MT1' && $oficina!='MTY') {
                        $sqlBuscaEMB.= "and oficina ='$oficina' ";
                    }
                    if (($oficina=='MTY' && $oficina != "TOD") || ($oficina=='MT1' && $oficina != "TOD")) {
                        $sqlBuscaEMB.= "and oficina IN ('MTY','MT1') ";
                    }
                }
                //ejecutar store que realiza un update a recoleccions para cerrarla y asigngar numero de guia
             //   $queryStore= $this->objSQL->setQuery("EXECUTE CERRAR_REC('$llega_orden','$llega_ofi','$oficina',$no);");

                //ejecutar sql para busqueda de datos en ordenes de emabarque para encontrar datos y asignalrlos al data del grid
                $sqlBuscaEMB = strtoupper($sqlBuscaEMB);
              //  echo $sqlBuscaEMB;
                $queryAux = $this->objSQL->setQueryAux($sqlBuscaEMB);
                 $recno_aux =$this->objSQL->getNumrowsAux($queryAux);
                 $cur_aux=0;
                while ($cur_aux<$recno_aux) {
              
                      

                    $arrayAux = $this->objSQL->getArrayAux($queryAux);
                    if($llega_orden=="485244")
                    {
                   // var_dump($arrayAux);
                    }
                  //asignar nuevos campos a arreglo de datos, para que se muestren en el grid
                     $lleg_guia = $arrayAux['NUMGUIA'];
                     if($lleg_guia>' '){
                      $data[$i]["FOLIO"]= $arrayAux['FOLIO']; //llega_embarque
                      $data[$i]["FECHAOEMB"]=$arrayAux['FECHAOEMB']; // fecha_embarque
                      $data[$i]["NUMGUIA"] =$arrayAux['NUMGUIA']; // llega_guia
                     }
                      $cur_aux++;  
                        
                        
                     // $llega_guia = $data[$i]["NUMGUIA"];
                     // $llega_embarque = $data[$i]["FOLIO"];
               }
            }
            /*
        if($llega_embarque>''){
            
                 $query_upd="UPDATE PITIC.SOLIC_RECOL SET CERRADA='1',GUIA='$llega_guia'  WHERE NUMERO='$no'";
                 $result=$this->objSQL->setQueryAux($query_upd);
        
            }
            */
        }
            //cuando sea pendiente el filtro hay que quitar del array $data todas las que tengan numero de guia
          if($estatus =="PEN"){

                  $array_conguia =array();
                    for($i=0;$i<count($data);$i++){
                        
                       $numguia= $data[$i]["NUMGUIA"];
                        if($numguia>' '){
                            $array_conguia[]=$i;
                          
                        }
                        
                    }
                    //recorrer ids array que hay que borrar ( son los que tienen numero de guia y no deben salir en pendientes)
                    for($x=0;$x<count($array_conguia);$x++){
                        $id_borrar = $array_conguia[$x];
                       // echo $id_borrar;
                        unset($data[$id_borrar]); 
                    }
                    $data = array_values($data);
                }else if($estatus =="3"){
                    //encontrar las que no tienen guia. se quitaran del array las que no tengan guia y que no tengan x.folio excep folio
                     $array_singuia =array();
                    for($i=0;$i<count($data);$i++){
                        
                       $numguia= $data[$i]["NUMGUIA"];
                        if($numguia<' '){
                            $array_singuia[]=$i;
                          
                        }
                        
                    }
                      //recorrer ids array que hay que borrar ( son los que NO tienen numero de guia y no deben salir en realizadas)
                    for($x=0;$x<count($array_singuia);$x++){
                        $id_borrar = $array_singuia[$x];
                       // echo $id_borrar;
                        unset($data[$id_borrar]); 
                    }
                    $data = array_values($data);

                }
     return $data;
    }

     /*CONSULTAR SI EL USUARIO TIENE ACTIVADAS TODAS EN ACCESOS DE RECOLECCIONES  */
     public function validar_oficina_todas($usuario){
 
        $sql="select * from pitic.recoleccion_cat_accesos where usuario ='$usuario' and OFICINA ='TOD' AND ACTIVO=1";
        $result = $this->objSQL->setQuery($sql);
        $recno =$this->objSQL->getNumrows($result);
     
        if($recno>0){
            return true;
        }
        return false;
  
    }

    public function validacionTipoUsuarios_sinRestricciones($usuario,$oficina){
        $num=0;
        $user = $usuario;
        $ofi=trim($oficina);
      // $user ="jdzib";
        $cadena= "{ 'succes': true,  'totalCount': '".intval($num)."'   , 'data' : [   ";
         $user=trim($user);
       
                        //USUARIO NORMAL SU  OFICINA
                            $sql = "SELECT * from guias.Oficina WHERE ACTIVADA=1 and OFIVENTAS IS NOT NULL ";
                        
                            $sql .= "  order by DESCRIPCION ";
                            $result = $this->objSQL->setQuery($sql);
                            $num = $this->objSQL->getNumrows($result);
                    
        if($num>0){
            
            for($i=0; $i<$num; $i++){
                 $renglon=$this->objSQL->getArray($resultado);
                 $clave = trim($renglon["OFICINA"]);					   	
                 $desc = $renglon["DESCRIPCION"];
                 
                 $cadena.= "{ 'OFICINA': '".$clave."', 'DESCRIPCION': '".trim($desc)."'  },";
                       
            }
            $cadena= trim($cadena, ',');
            echo $cadena.= "  ] }  ";
        }
    
    }
    public function validacionTipoUsuarios($usuario,$oficina){
        $num=0;
        $user = $usuario;
        $ofi=trim($oficina);
      // $user ="jdzib";
        $cadena= "{ 'succes': true,  'totalCount': '".intval($num)."'   , 'data' : [   ";
         $user=trim($user);
        
                //ACCESO A GERENTES  REGIONALES
                $sql = "  SELECT B.REGION_VTAS,B.OFICINA, B.DESCRIPCION FROM CATALOGOS.GER_REGIONAL A ";
                $sql .= " LEFT JOIN GUIAS.OFICINA B ON A.REGION = B.REGION_VTAS ";
                $sql .= " WHERE A.USUARIOWEB = '".$user."' and b.ACTIVADA=1 and  b.OFIVENTAS IS NOT NULL  order by b.DESCRIPCION ";
                //echo $sql;
                $result = $this->objSQL->setQuery($sql);
                 $num = $this->objSQL->getNumrows($result);
    
                if($num<=0){	
                //ACCESO A GERENTES DE OFICINA		
                     $sql = "SELECT  A.USUARIOWEB,B.OFICINA, B.DESCRIPCION FROM CATALOGOS.GER_OFICINA A ";
                     $sql .= " LEFT JOIN GUIAS.OFICINA B ON A.OFICINA = B.OFICINA ";
                     $sql .= " WHERE A.USUARIOWEB = '".$user."' AND b.ACTIVADA=1  AND A. ACTIVO='1' order by B.DESCRIPCION";
                            //echo $sql;
                        $result = $this->objSQL->setQuery($sql);
                        $num = $this->objSQL->getNumrows($result);
                        $cur=1;
                        if ($num <= 0){ 
                        //USUARIO NORMAL SU  OFICINA
                            $sql = "SELECT * from guias.Oficina WHERE ACTIVADA=1 and OFIVENTAS IS NOT NULL ";
                        
                                //ASI EL USUARIO NO TIENE ACTIVADAS TODAS LAS OFICINAS
                                if(!$this->validar_oficina_todas($user)){ // usuarios que tienen agregadas todas las oficinas en modulo de accesos recolecciones
                               // $oficina = $_COOKIE["oficina_plight"];
                                if($oficina =='') $_COOKIE["oficina_plight"];
                                $sql .= "and oficina = '$oficina'";
                                $buscavende="T";
                            }
                            $sql .= "  order by DESCRIPCION ";
                            $result = $this->objSQL->setQuery($sql);
                            $num = $this->objSQL->getNumrows($result);
                        }else{
                            $vendcom = 1;
                        }
                }else{
                    $coord = 1;
                }
        
        if($num>0){
            
            for($i=0; $i<$num; $i++){
                 $renglon=$this->objSQL->getArray($resultado);
                 $clave = trim($renglon["OFICINA"]);					   	
                 $desc = $renglon["DESCRIPCION"];
                 
                 $cadena.= "{ 'OFICINA': '".$clave."', 'DESCRIPCION': '".trim($desc)."'  },";
                       
            }
            $cadena= trim($cadena, ',');
            echo $cadena.= "  ] }  ";
        }
    
    }
    function registrarLog($tipo,$oficina_buscada,$fechai,$fechaf){
        $usuario = $_COOKIE["usuario_plight"];
        $oficina_usuario = $_COOKIE["oficina_plight"]; 
        $sql_log = "INSERT INTO PITIC.RECOL_LOGS VALUES((select max(ID) + 1 as ID  from PITIC.RECOL_LOGS),'$usuario','$oficina_usuario',SYSDATE,SYSTIMESTAMP,'REPORTE_SOLICITUD','$tipo','$oficina_buscada','$fechai','$fechaf')";
        $query2 = $this->objSQL->setQuery($sql_log);
    }
    function cerrarConexion()
    {
        $this->objSQL->close($this->db);
    }
}
