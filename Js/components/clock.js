export default function Clock() {
  var updateClock, task;
  updateClock = function () {
    Ext.getCmp("clock").setValue(Ext.Date.format(new Date(), "g:i:s A"));
  };
  task = Ext.TaskManager.start({
    run: updateClock,
    interval: 1000,
  });

  return task;
}
