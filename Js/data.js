// MODELS
Ext.define("data_regiones", {
  extend: "Ext.data.Model",
  fields: [{ name: "REGION_VTAS", type: "string" }],
});

Ext.define("Oficinas", {
  extend: "Ext.data.Model",
  fields: [
    { name: "OFICINA", type: "string" },
    { name: "DESCRIPCION", type: "string" },
  ],
});

Ext.define("Reporte", {
  extend: "Ext.data.Model",
  fields: [
    { name: "PREMIUM", type: "int" },
    { name: "NUMERO", type: "int" },
    { name: "NUMEORDEN", type: "int" },
    { name: "CHOFASIG", type: "string" },
    { name: "CVE_CHOFERLLEGA", type: "int" },
    { name: "FECHALLEGA", type: "string" },
    { name: "HORALLEGA", type: "int" },
    { name: "GEOPOS", type: "string" },
    { name: "GEOPOS_EXCEP", type: "string" },
    { name: "HORA_EXCEP", type: "string" },
    { name: "ORMLLEGA", type: "string" },
    { name: "OFILLEGA", type: "string" },
    { name: "FOLIOLLEGA", type: "int" },
    { name: "OFICINA_RECOLEC", type: "string" },
    { name: "GUIA", type: "string" },
    { name: "FECHA", type: "string" },
    { name: "CIUDAD", type: "string" },
    { name: "NOMCLIENTE", type: "string" },
    { name: "DOMICILIOYNUM", type: "string" },
    { name: "TEL_RECOLEC", type: "string" },
    { name: "HORA_RECOLEC", type: "string" },
    { name: "HORAINICIOFIN", type: "string" },
    { name: "HORA_RECOLECINI", type: "string" },
    { name: "HORA_RECOLECFIN", type: "string" },
    { name: "FECHA_RECOLEC", type: "string" },
    { name: "CANCELADA", type: "int" },
    { name: "REVISADA", type: "int" },
    { name: "ASIGNADA", type: "int" },
    { name: "DESTINATARIO", type: "string" },
    { name: "CAPTURO", type: "string" },
    { name: "FOLIO", type: "string" },
    { name: "RECIBIDA", type: "int" },
  ],
});

const Store = (model, accion, autoload, root = "") => {
  return Ext.create("Ext.data.Store", {
    model: model,
    proxy: {
      type: "ajax",
      url: "./app/funciones.php",
      actionMethods: {
        create: "POST",
        read: "POST",
        update: "POST",
        destroy: "POST",
      },
      extraParams: { eid: "0", accion: accion },
      reader: {
        type: "json",
        root: root,
      },
    },
    autoLoad: autoload,
  });
};

export const opciones_estado = Ext.create("Ext.data.Store", {
  fields: ["value", "text"],
  data: [
    { value: "TOD", text: "Todas" },
    { value: "0", text: "Recibidas" },
    { value: "1", text: "Revisadas" },
    { value: "2", text: "Asignadas" },
    { value: "PEN", text: "Pendientes de recolección" },
    { value: "3", text: "Realizada/Cerrada" },
    { value: "4", text: "Canceladas" },
  ],
});

// STORES
export const opciones_region = Store("data_regiones", "getRegiones", true);
export const store_oficinas = Store("Oficinas", "getOficinas", true, "data");
export const myStore = Store("Reporte", "consultar", false);
