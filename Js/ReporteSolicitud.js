import Clock from "./components/clock.js";
import {
  myStore,
  opciones_estado,
  opciones_region,
  store_oficinas,
} from "./data.js";

var totpremium = 0;
var totasignadas = 0;
var totrevisadas = 0;
var totefectuadas = 0;
var totcanceladas = 0;
var totalconguia = 0;
function pop_up(num) {
  URL =
    "https://www.tpitic.com.mx/piticlight/oracle/Version2/Recoleccion/detalle_destino.php?nosolicitud=" +
    num;
  id = 1;
  eval(
    "page" +
      id +
      " = window.open(URL, '" +
      id +
      "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=600,height=200,left = 400,top = 200');"
  );
}

Ext.Loader.setPath("Ext.ux.exporter", "Ext.ux.exporter"); // Only the Ext.ux.exporter.* classes will be searched in ./something/exporter'
Ext.require(["Ext.tree.*", "Ext.data.*", "Ext.window.MessageBox"]);
Ext.require([
  "Ext.tab.*",
  "Ext.window.*",
  "Ext.tip.*",
  "Ext.layout.container.Border",
  "Ext.util.TaskRunner.*",
  "Ext.grid.feature.Summary",
]);
Ext.onReady(function () {
  function ajustecolumna(val) {
    return '<div style="white-space:normal !important;">' + val + "</div>";
  }
  function ajustedomicilio(val) {
    return (
      '<div style="white-space:normal !important;font-size:11px">' +
      val +
      "</div>"
    );
  }
  function ajustefecha_recolec(val, metaData) {
    var hora = metaData.record.get("HORAINICIOFIN");
    var hora = hora.split("-");

    var hora1 = hora[0].substring(0, 5);
    var hora2 = hora[1].substring(0, 5);
    return (
      '<div style="white-space:normal !important;font-size:11px;margin-bottom:5px;">' +
      val +
      "<br>" +
      hora1 +
      "-" +
      hora2 +
      "</div>"
    );
  }
  function ajustepremium(val) {
    if (val == 1) {
      totpremium++;
      return '<div style="white-space:normal !important;"> <img src="./assets/imgs/knewstuff.png"></div>';
    } else return "";
  }
  function ajustesolicitud(val) {
    return (
      "<a target='_blank' href='https://www.tpitic.com.mx/piticlight/oracle/Version2/Recoleccion/consultaindividual.php?nosolicitud=" +
      val +
      "'>" +
      val +
      "</a>"
    );
  }
  function ajusterecibida() {
    return '<div style="white-space:normal !important;"> <img src="./assets/imgs/checkclear.gif"></div>';
  }
  function ajusteguia(val) {
    if (val != "") {
      //      totalconguia = totalconguia + 1;
    }

    return (
      "<a target='_blank' href='https://www.tpitic.com.mx/empleados/Reportes/Guias/guiasobserva.php?txtguia=" +
      val +
      "'>" +
      val +
      "</a>"
    );
  }
  function ajusterevisada(val, metaData) {
    var no = metaData.record.get("NUMERO");
    if (val == 1) {
      totrevisadas++;
      return (
        '<div style="white-space:normal !important;">' +
        "<a target='_blank' href='https://www.tpitic.com.mx/piticlight/oracle/Version2/Recoleccion/capenviounidad.php?solicitud=" +
        no +
        "&envia=T'>" +
        '<img src="./assets/imgs/checkjaune.gif"></div></a>'
      );
    } else {
      return "";
    }
  }
  function ajusterecdigital(val, metaData) {
    var chof = metaData.record.get("CHOFASIG");
    var posiciones;

    if (typeof val != "undefined" && val != null && val != "") {
      totefectuadas = totefectuadas + 1;
      posiciones = val.split(",");
      return (
        '<div style="white-space:normal !important;height:19px;">' +
        ' <img src="./assets/imgs/check.gif"></div> ' +
        '<div style="white-space:normal !important;">' +
        "<a target='_blank' href='https://www.tpitic.com.mx/android_web/testmap/posicionmapapr.php?lat=" +
        posiciones[0] +
        "&long=" +
        posiciones[1] +
        "'>" +
        chof +
        "</a></div>"
      );
    } else return "";
  }
  function ajusteexcepdigital(val, metaData) {
    // var chof = metaData.record.get("CHOFASIG");
    var hora = metaData.record.get("HORA_EXCEP").substring(0, 5);
    var posiciones;

    if (typeof val != "undefined" && val != null && val != "") {
      posiciones = val.split(",");
      return (
        '<div style="white-space:normal !important;height:19px;">' +
        ' <img src="./assets/imgs/checkrojo.gif"> </div>' +
        '<div style="white-space:normal !important;">' +
        "<a target='_blank' href='https://www.tpitic.com.mx/android_web/testmap/posicionmapapr.php?lat=" +
        posiciones[0] +
        "&long=" +
        posiciones[1] +
        "'>" +
        hora +
        "</a></div>"
      );
    } else return "";
  }
  function ajustecerraconoe(val) {
    if (typeof val != "undefined" && val != null && val != "") {
      return (
        '<div style="white-space:normal !important;height:19px;">' +
        ' <img src="./assets/imgs/check.gif"></div>' +
        '<div style="white-space:normal !important;">' +
        val +
        "</div> "
      );
    } else return "";
  }
  function ajustedestinatario(val, metaData) {
    var no = metaData.record.get("NUMERO");
    return (
      '<div style="white-space:normal !important;">' +
      "<a target='_blank'  href='javascript:pop_up(" +
      no +
      ")'>" +
      "</div>" +
      val +
      "</a>"
    );
  }

  function ajusteasignada(val, metaData) {
    var no = metaData.record.get("NUMERO");
    if (val == 1) {
      totasignadas++;
      return (
        '<div style="white-space:normal !important;"> ' +
        "<a href='https://www.tpitic.com.mx/piticlight/oracle/Version2/Recoleccion/cierrasoli.php?nosolicitud=" +
        no +
        "&envia=T'>" +
        ' <img src="./assets/imgs/checkblue.gif"></a></div>'
      );
    } else {
      return "";
    }
  }
  function ajustecancelada(val) {
    if (val == 1) {
      totcanceladas++;
      return '<div style="white-space:normal !important;"> <img src="./assets/imgs/checkrojo.gif"></div>';
    } else {
      return "";
    }
  }

  function ajustenumeorden(val) {
    if (val == 0) {
      return "";
    } else return val;
  }

  var f = new Date();

  var dia = f.getDate();
  if (dia < 10) dia = "0" + dia;
  var mes = f.getMonth() + 1;
  if (mes < 10) mes = "0" + mes;

  const ChkBox = (id, boxlabel) => {
    return new Ext.form.field.Checkbox({
      id: id,
      xtype: "checkbox",
      boxLabel: boxlabel,
      labelSeparator: "",
      value: 0,
      labelWidth: 120,
      style: "",
    });
  };

  const chk_icanceladas = ChkBox("canceladas", "Incluir canceladas");
  const chk_singuia = ChkBox("singuia", "Pendientes de Guía");
  const chk_fechacaptura = ChkBox("feccap", "Por fecha de captura");

  var text = new Ext.Component({
    style: "background:#FFFFD9;margin-top:10px",
    html: "Estatus de la solicitud: ",
  });

  var text_totalpremium = new Ext.form.Label({
    id: "lblTotal",
    text: "0",
  });
  var text_totalrevisadas = new Ext.form.Label({
    id: "lblTotalRevisadas",
    text: "0",
  });
  var text_totalasignadas = new Ext.form.Label({
    id: "lblTotalAsignadas",
    text: "0",
  });
  var text_totalefectuadas = new Ext.form.Label({
    id: "lblTotalEfectuadas",
    text: "0",
  });
  var text_totalcanceladas = new Ext.form.Label({
    id: "lblTotalCanceladas",
    text: "0",
  });

  var text_mensaje = new Ext.form.Label({
    style: "color:#333399;display: block;text-align:center",
    text: "- El rango de fechas se basa en la fecha de recoleccion",
  });

  var text_hora = new Ext.form.TextField({
    labelSeparator: "",
    fieldLabel: "Hora",
    id: "clock",
    readOnly: true,
  });

  opciones_region.reload({
    params: {
      accion: "getRegiones",
    },
  });

  Ext.Ajax.request({
    url: "./app/funciones.php",
    params: {
      accion: "getInfoSesion",
    },
    success: function (response) {
      var text = JSON.parse(response.responseText);
      text_usuario.setValue(text.usuario);
    },
  });

  // Create the combo box, attached to the states data store
  var combo_region = Ext.create("Ext.form.ComboBox", {
    id: "combo_region",
    store: opciones_region,
    queryMode: "local",
    valueField: "REGION_VTAS",
    value: "",
  });

  var combo_estado = Ext.create("Ext.form.ComboBox", {
    id: "combo_estado",
    store: opciones_estado,
    queryMode: "local",
    valueField: "value",
    value: "TOD",
  });

  var comboboxOficina = Ext.create("Ext.form.ComboBox", {
    width: 45,
    labelWidth: 50,
    store: store_oficinas,
    labelSeparator: " ",
    fieldLabel: "Oficina",
    id: "oficina",
    displayField: "DESCRIPCION",
    valueField: "OFICINA",
    queryMode: "local",
    emptyText: "Selecciona oficina...",
  });

  var text_usuario = new Ext.form.TextField({
    width: 50,
    labelSeparator: "",
    id: "text_usuario",
    fieldLabel: "Usuario",
    value: "iperez",
    style: "background:#FFFFD9;border:1px solid #FFB54D;",
    readOnly: true,
  });

  const DateField = (fieldLabel) => {
    return new Ext.form.DateField({
      xtype: "datefield",
      anchor: "100%",
      fieldLabel: fieldLabel,
      labelWidth: 80,
      name: "from_date",
      value: new Date(),
      style: "background:#FFFFD9;",
    });
  };

  const fec1 = DateField("Fecha inicial");
  const fec2 = DateField("Fecha Final");

  var btn_Ejecutar = new Ext.Button({
    // text: '<center><img style="10px" src="./assets/imgs/buscar.png" /></center><b>Ejecutar</b>',
    text: "BUSCAR",
    width: 100,
    height: 35,
    handler: function () {
      if (Ext.getCmp("oficina").getValue() == null) {
        Ext.MessageBox.alert("Mensaje", "Selecciona la oficina");
        return false;
      }
      totpremium = 0;
      totasignadas = 0;
      totrevisadas = 0;
      totefectuadas = 0;
      totcanceladas = 0;
      totalconguia = 0;

      if (chk_fechacaptura.getValue() == true) {
        text_mensaje.setText(
          "- El rango de fechas se basa en la fecha de captura"
        );
        text_mensaje.addCls("red");
        text_mensaje.removeCls("blue");
      } else {
        text_mensaje.setText(
          "- El rango de fechas se basa en la fecha de recoleccion"
        );
        text_mensaje.addCls("blue");
        text_mensaje.removeCls("red");
      }

      myStore.load({
        params: {
          accion: "consultar",
          id: 1,
          incluircanceladas: chk_icanceladas.getValue(),
          pendienteguia: chk_singuia.getValue(),
          estado: combo_region.getValue(),
          oficina: comboboxOficina.getValue(),
          estatussolicitud: combo_estado.getValue(),
          fechacaptura: chk_fechacaptura.getValue(),
          fechainicial: Ext.Date.format(fec1.getValue(), "Y-m-d"),
          fechafinal: Ext.Date.format(fec2.getValue(), "Y-m-d"),
        },
        callback: function (records, operation, success) {
          // do something after the load finishes
          Ext.getCmp("grid").getView().refresh();
        },
        scope: this,
      });
    },
  });

  var dr = new Ext.FormPanel({
    frame: true,
    style: "text-align:left; padding: 1.5em 1.2em;",
    title: "-REPORTE DE SOLICITUDES DE RECOLECCION-",
    width: 650,
    // height: 240,
    items: [
      {
        layout: "column",
        renderer: Ext.util.Format.uppercase,
        items: [
          {
            columnWidth: 0.36,
            layout: "form",
            border: false,
            items: [text_hora, comboboxOficina, fec1],
          },
          {
            columnWidth: 0.36,
            layout: "form",
            border: false,
            items: [text, combo_estado, fec2],
          },
          {
            columnWidth: 0.23,
            layout: "form",
            border: false,
            items: [chk_icanceladas, chk_fechacaptura, chk_singuia],
          },
        ],
      },
      {
        layout: "column",
        items: [
          {
            cls: "btn-n-txt",
            height: 70,
            columnWidth: 1,
            layout: "form",
            border: false,
            labelWidth: 36,
            style: "margin-top:10px",
            items: [btn_Ejecutar, text_mensaje],
          },
        ],
      },
    ],

    // ],
  });
  dr.render("dr");
  text_totalpremium.render("h2total");

  //GRID
  var grid = new Ext.grid.GridPanel({
    store: myStore,
    xtype: "locking-grid",
    columnLines: true,
    loadMask: true,
    autoScroll: true,
    frame: true,
    title: "Listado",
    id: "grid",
    width: 1280,
    height: 550,
    autoHeight: true,
    autoWidth: true,
    trackMouseOver: true,
    stripeRows: true,
    scrollFlags: {
      x: true,
    },
    enableDragDrop: false,
    enableColumnMove: true,
    enableHdMenu: false,
    features: [
      {
        ftype: "summary",
      },
    ],
    tbar: [
      {
        text: "Bajar  informacion",
        scale: "medium",
        glyph: "xf019@FontAwesome",
        handler: function () {
          var recordsToSend = [];
          selected = Ext.getCmp("grid").getView().getSelectionModel().store
            .data.items;
          Ext.each(selected, function (item) {
            recordsToSend.push(item.data);
          });
          if (recordsToSend.length > 0) {
            recordsToSend = Ext.JSON.encode(recordsToSend);
            Ext.Ajax.request({
              url: "./app/ExportData.php",
              params: {
                griddatos: recordsToSend,
              },
              success: function (response, request) {
                var html = response.responseText;
                var win = window.open("", "printgrid");
                win.document.write(html);
                win.document.close();
                win.focus();
                //win.print();
                //win.close();
              },
              failure: function (result, request) {},
            });
          } else {
            alert("Debes seleccionar un campo a imprimir.");
          }
        },
      },
    ],
    columns: [
      {
        header: '<center style="font-size:9px;"><B>No.</B>',
        align: "center",
        xtype: "rownumberer",
        locked: true,
        sortable: false,
        autoSizeColumn: true,
      },
      {
        header: '<center style="font-size:9px"><B>Cliente<br>Premium</B>',
        locked: true,
        autoSizeColumn: true,
        dataIndex: "PREMIUM",
        width: 45,
        align: "center",
        renderer: ajustepremium,
      },
      {
        header: '<center style="font-size:9px"><B>Solicitud</B>',
        locked: true,
        autoSizeColumn: true,
        dataIndex: "NUMERO",
        width: 45,
        align: "center",
        renderer: ajustesolicitud,
      },
      {
        header: '<center style="font-size:9px"><B>Fecha <br>Captura</B>',
        locked: true,
        autoSizeColumn: true,
        dataIndex: "FECHA",
        width: 60,
        align: "center",
      },
      {
        header: '<center style="font-size:9px"><B>Oficina</B>',
        locked: true,
        autoSizeColumn: true,
        dataIndex: "OFICINA_RECOLEC",
        width: 40,
        align: "center",
      },
      {
        header: '<center style="font-size:9px"><B>Ciudad</B>',
        locked: true,
        autoSizeColumn: true,
        dataIndex: "CIUDAD",
        width: 40,
        align: "center",
      },
      {
        header: '<center style="font-size:9px"><B>Nombre de Cliente</B>',
        locked: true,
        autoSizeColumn: true,
        dataIndex: "NOMCLIENTE",
        width: 135,
        align: "center",
        renderer: ajustedomicilio,
      },
      {
        header: '<center style="font-size:9px"><B>Domicilio</B>',
        locked: true,
        autoSizeColumn: true,
        dataIndex: "DOMICILIOYNUM",
        width: 145,
        align: "center",
        renderer: ajustedomicilio,
      },
      {
        header: '<center style="font-size:9px"><B>Telefono</B>',
        locked: true,
        autoSizeColumn: true,
        dataIndex: "TEL_RECOLEC",
        width: 65,
        autoWidth: true,
        align: "center",
        renderer: ajustecolumna,
      },
      {
        header: '<center style="font-size:9px"><B>Fecha de<br>Recolección</B>',
        locked: true,
        autoSizeColumn: true,
        dataIndex: "FECHA_RECOLEC",
        width: 65,
        align: "center",
        renderer: ajustefecha_recolec,
      },
      // { header: '<center style="font-size:9px"><B>Horario</B>', dataIndex: 'HORAINICIOFIN', width: 60, align: 'center', renderer: ajustehora },
      {
        header: '<center style="font-size:9px"><B>Capturo</B>',
        autoSizeColumn: true,
        dataIndex: "CAPTURO",
        width: 70,
        align: "center",
        summaryRenderer: function (v, p, d) {
          return '<B><span align="left" style="color:#333333;font-size:13px">TOTALES</span></B>';
        },
      },
      {
        header: '<center style="font-size:9px"><B>Datos del<br>Destino</B>',
        autoSizeColumn: true,
        dataIndex: "DESTINATARIO",
        width: 60,
        align: "center",
        renderer: ajustedestinatario,
      },
      {
        header: '<center style="font-size:9px"><B>Recibida</B>',
        autoSizeColumn: true,
        dataIndex: "RECIBIDA",
        width: 45,
        align: "center",
        renderer: ajusterecibida,
        summaryType: "count",
        summaryRenderer: function (value) {
          return (
            '<B><span align="left" style="color:#333333;font-size:13px">' +
            value +
            "</span></B>"
          );
        },
      },
      {
        header: '<center style="font-size:9px"><B>Revisada</B>',
        autoSizeColumn: true,
        dataIndex: "REVISADA",
        width: 45,
        align: "center",
        renderer: ajusterevisada,
        summaryType: "sum",
        summaryRenderer: function (value) {
          return (
            '<B><span align="left" style="color:#333333;font-size:13px">' +
            value +
            "</span></B>"
          );
        },
      },
      {
        header: '<center style="font-size:9px"><B>Asignada</B>',
        autoSizeColumn: true,
        dataIndex: "ASIGNADA",
        width: 45,
        align: "center",
        renderer: ajusteasignada,
        summaryType: "sum",
        summaryRenderer: function (value) {
          return (
            '<B><span align="left" style="color:#333333;font-size:13px">' +
            value +
            "</span></B>"
          );
        },
      },
      {
        header: '<center style="font-size:9px"><B>Orden<br>Recolección</B>',
        autoSizeColumn: true,
        dataIndex: "NUMEORDEN",
        width: 55,
        align: "center",
        renderer: ajustenumeorden,
      },
      {
        header: '<center style="font-size:9px"><B>Chofer<br>Recol</B>',
        autoSizeColumn: true,
        dataIndex: "CHOFASIG",
        width: 45,
        align: "center",
      },
      {
        header: '<center style="font-size:9px"><B>Rec Digital<br>Efectuada</B>',
        dataIndex: "GEOPOS",
        width: 58,
        align: "center",
        renderer: ajusterecdigital,
      },
      {
        header: '<center style="font-size:9px"><B>Excepcion<br>Digital</B>',
        autoSizeColumn: true,
        dataIndex: "GEOPOS_EXCEP",
        width: 55,
        align: "center",
        renderer: ajusteexcepdigital,
      },
      {
        header: '<center style="font-size:9px"><B>Cerrada<br>Con O/E</B>',
        autoSizeColumn: true,
        dataIndex: "FOLIO",
        width: 60,
        align: "center",
        renderer: ajustecerraconoe,
      },
      {
        header: '<center style="font-size:9px"><B>Can<br>celada</B>',
        autoSizeColumn: true,
        dataIndex: "CANCELADA",
        width: 40,
        align: "center",
        renderer: ajustecancelada,
        summaryType: "sum",
        summaryRenderer: function (value) {
          return (
            '<B><span align="left" style="color:#333333;font-size:13px">' +
            value +
            "</span></B>"
          );
        },
      },
      {
        header: '<center style="font-size:9px"><B>ORM</B>',
        autoSizeColumn: true,
        dataIndex: "ORMLLEGA",
        width: 50,
        align: "center",
      },
      {
        header: '<center style="font-size:9px"><B>GUIA</B>',
        autoSizeColumn: true,
        dataIndex: "GUIA",
        width: 55,
        align: "center",
        renderer: ajusteguia,
        summaryType: "count",
        summaryRenderer: function (value) {
          return (
            '<B><span align="left" id="total_guias" style="color:#333333;font-size:13px">' +
            "0" +
            "</span></B>"
          );
        },
      },
    ],
    viewConfig: {
      //interface
      emptyText: "---NO SE ENCONTRARON RESULTADOS---",
      //   forceFit: true, //para que se extienda la inf y no aparezca el scroll horizontal <->
      scrollOffset: 0, //borra el espacio reservado para la barra vertical
      stripeRows: true,
    },
  });
  //grid.hide(true);
  grid.render("topic-grid");

  grid.store.on(
    "load",
    function (store, records, options) {
      text_totalpremium.setText(totpremium);
      text_totalasignadas.setText(totasignadas);
      text_totalrevisadas.setText(totrevisadas);
      text_totalefectuadas.setText(totefectuadas);
      text_totalcanceladas.setText(totcanceladas);
      var fields = grid.store.model.getFields();
      //falta ocultar columna
      myStore.each(function (rec) {
        var guia = rec.get("GUIA");
        if (guia != "") {
          totalconguia = totalconguia + 1;
        }
      });

      Ext.getCmp("grid").getView().refresh();
      Ext.getCmp("grid").getView().refresh();
      setTimeout(function () {
        document.getElementById("total_guias").textContent = totalconguia;
      }, 1500);
    },
    grid
  );

  Clock();
});
